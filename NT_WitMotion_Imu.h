/*
 * @file NT_WitMotion_Imu.h
 * @author Nicolas Toupin
 * @date 2019-11-24
 * @brief WitMotion Imu function declarations.
 */

#ifndef NT_WitMotion_Imu_h

#include "Arduino.h"

#define TRUE 1
#define FALSE 0

#define IMU_BYTE_QTY 10

#define NT_WitMotion_Imu_h

class Accelerometer_t
{
public:
	bool Enable = FALSE;
	int Command = 0x51;
	float X_Value = 0;
	float Y_Value = 0;
	float Z_Value = 0;
};

class Angular_Velocity_t
{
public:
	bool Enable = FALSE;
	int Command = 0x52;
	float X_Value = 0;
	float Y_Value = 0;
	float Z_Value = 0;
};

class Angle_t
{
public:
	bool Enable = FALSE;
	int Command = 0x53;
	float X_Value = 0;
	float Y_Value = 0;
	float Z_Value = 0;
};

class Magnetometer_t
{
public:
	bool Enable = FALSE;
	int Command = 0x54;
	float X_Value = 0;
	float Y_Value = 0;
	float Z_Value = 0;
};

class WitMotion_Imu_t
{
public:
	WitMotion_Imu_t();
	bool Enable = FALSE;
	bool New_Data_Flag = FALSE;
	Accelerometer_t Accelerometer;
	Angular_Velocity_t Angular_Velocity;
	Angle_t Angle;
	Magnetometer_t Magnetometer;
	void Configure();
	void Attach(HardwareSerial* Serial_Port, int Baud_Rate);
	void Init();
	void Deinit();
	void Serial_Receive();
	void Serial_Print(Stream& Serial_Port);
	void Serial_Print(HardwareSerial* Serial_Port_Tmp);

private:
	HardwareSerial* _Serial_Port;
	int _Baud_Rate;
	int _Sync_Byte = 0x55;
	long _Execution_Time = 0;
	int _Buffer[IMU_BYTE_QTY] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
};

#endif