/*
   @file WitMotion_Imu.ino
   @author Nicolas Toupin
   @date 2019-11-24
   @brief Simple example of WitMotion Imu utilisation.
*/

#include <NT_WitMotion_Imu.h>

WitMotion_Imu_t Imu_1;

void setup()
{
  Imu_1.Configure();
  Imu_1.Attach(&Serial1, 9600);
  Imu_1.Init();
}

void loop()
{
  Imu_1.Serial_Receive();

  if (Imu_1.New_Data_Flag == TRUE)
  {
    Imu_1.Serial_Print(Serial);
    Imu_1.New_Data_Flag = FALSE;
  }
}
