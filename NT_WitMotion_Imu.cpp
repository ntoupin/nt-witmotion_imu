/*
 * @file NT_WitMotion_Imu.cpp
 * @author Nicolas Toupin
 * @date 2019-11-24
 * @brief Led function definitions.
 */

#include "Arduino.h"
#include "NT_WitMotion_Imu.h"

WitMotion_Imu_t::WitMotion_Imu_t()
{
}

void WitMotion_Imu_t::Configure()
{

}

void WitMotion_Imu_t::Attach(HardwareSerial* Serial_Port, int Baud_Rate)
{
	_Serial_Port = Serial_Port;
	_Baud_Rate = Baud_Rate;
}

void WitMotion_Imu_t::Init()
{
	Enable = TRUE;
	Accelerometer.Enable = TRUE;
	Angular_Velocity.Enable = TRUE;
	Angle.Enable = TRUE;
	Magnetometer.Enable = TRUE;

	_Serial_Port->begin(_Baud_Rate);
}

void WitMotion_Imu_t::Deinit()
{
	Enable = FALSE;
	Accelerometer.Enable = FALSE;
	Angular_Velocity.Enable = FALSE;
	Angle.Enable = FALSE;
	Magnetometer.Enable = FALSE;

	_Serial_Port->end();
}

void WitMotion_Imu_t::Serial_Receive()
{
	if (Enable == TRUE)
	{
		if (_Serial_Port->available() > 0)
		{
			long Timestamp = micros();

			// Wait for flag beginning of package flag
			while (_Serial_Port->read() != _Sync_Byte);

			// Wait to have BYTE_QTY bytes into buffer
			while (_Serial_Port->available() < IMU_BYTE_QTY);

			// Fill buffer
			for (int i = 0; i < IMU_BYTE_QTY; ++i)
			{
				_Buffer[i] = _Serial_Port->read();
			}

			// Parse data
			if (_Buffer[0] == Accelerometer.Command)
			{
				if (Accelerometer.Enable == TRUE)
				{
					Accelerometer.X_Value = (int(_Buffer[2] << 8 | _Buffer[1])) / 32768.0 * 16;
					Accelerometer.Y_Value = (int(_Buffer[4] << 8 | _Buffer[3])) / 32768.0 * 16;
					Accelerometer.Z_Value = (int(_Buffer[6] << 8 | _Buffer[5])) / 32768.0 * 16;
				}
				else
				{
					Accelerometer.X_Value = 0;
					Accelerometer.Y_Value = 0;
					Accelerometer.Z_Value = 0;
				}
			}

			if (_Buffer[0] == Angular_Velocity.Command)
			{
				if (Angular_Velocity.Enable == TRUE)
				{
					Angular_Velocity.X_Value = (int(_Buffer[2] << 8 | _Buffer[1])) / 32768.0 * 2000;
					Angular_Velocity.Y_Value = (int(_Buffer[4] << 8 | _Buffer[3])) / 32768.0 * 2000;
					Angular_Velocity.Z_Value = (int(_Buffer[6] << 8 | _Buffer[5])) / 32768.0 * 2000;
				}
				else
				{
					Angular_Velocity.X_Value = 0;
					Angular_Velocity.Y_Value = 0;
					Angular_Velocity.Z_Value = 0;
				}
			}

			if (_Buffer[0] == Angle.Command)
			{
				if (Angle.Enable == TRUE)
				{
					Angle.X_Value = (int(_Buffer[2] << 8 | _Buffer[1])) / 32768.0 * 180;
					Angle.Y_Value = (int(_Buffer[4] << 8 | _Buffer[3])) / 32768.0 * 180;
					Angle.Z_Value = (int(_Buffer[6] << 8 | _Buffer[5])) / 32768.0 * 180;
				}
				else
				{
					Angle.X_Value = 0;
					Angle.Y_Value = 0;
					Angle.Z_Value = 0;
				}
			}

			if (_Buffer[0] == Magnetometer.Command)
			{
				if (Angle.Enable == TRUE)
				{
					Magnetometer.X_Value = (int(_Buffer[2] << 8 | _Buffer[1])) / 32768.0 * 180;
					Magnetometer.Y_Value = (int(_Buffer[4] << 8 | _Buffer[3])) / 32768.0 * 180;
					Magnetometer.Z_Value = (int(_Buffer[6] << 8 | _Buffer[5])) / 32768.0 * 180;
				}
				else
				{
					Magnetometer.X_Value = 0;
					Magnetometer.Y_Value = 0;
					Magnetometer.Z_Value = 0;
				}
			}

			long Delta = micros() - Timestamp;
			_Execution_Time = (float)Delta / 1000;

			New_Data_Flag = TRUE;
		}
	}
	else
	{
		_Serial_Port->flush();
	}
}

void WitMotion_Imu_t::Serial_Print(Stream& Serial_Port)
{
	Serial_Port.print("IMU RX -> ");

	Serial_Port.print("Acc. (deg/s2): ");
	Serial_Port.print(Accelerometer.X_Value); Serial_Port.print(", ");
	Serial_Port.print(Accelerometer.Y_Value); Serial_Port.print(", ");
	Serial_Port.print(Accelerometer.Z_Value);

	Serial_Port.print(" / Gyro. (deg/s): ");
	Serial_Port.print(Angular_Velocity.X_Value); Serial_Port.print(", ");
	Serial_Port.print(Angular_Velocity.Y_Value); Serial_Port.print(", ");
	Serial_Port.print(Angular_Velocity.Z_Value);

	Serial_Port.print(" / Incl. (deg): ");
	Serial_Port.print(Angle.X_Value); Serial_Port.print(", ");
	Serial_Port.print(Angle.Y_Value); Serial_Port.print(", ");
	Serial_Port.print(Angle.Z_Value);

	Serial_Port.print(" / Magn. (deg): ");
	Serial_Port.print(Magnetometer.X_Value); Serial_Port.print(", ");
	Serial_Port.print(Magnetometer.Y_Value); Serial_Port.print(", ");
	Serial_Port.print(Magnetometer.Z_Value);

	Serial_Port.print(" / ");
	Serial_Port.print("Rec. time (ms): ");
	Serial_Port.print(_Execution_Time);
	Serial_Port.println(" ms");
}

void WitMotion_Imu_t::Serial_Print(HardwareSerial* Serial_Port_Tmp)
{
	HardwareSerial Serial_Port = *Serial_Port_Tmp;

	Serial_Port.print("IMU RX -> ");

	Serial_Port.print("Acc. (deg/s2): ");
	Serial_Port.print(Accelerometer.X_Value); Serial_Port.print(", ");
	Serial_Port.print(Accelerometer.Y_Value); Serial_Port.print(", ");
	Serial_Port.print(Accelerometer.Z_Value);

	Serial_Port.print(" / Gyro. (deg/s): ");
	Serial_Port.print(Angular_Velocity.X_Value); Serial_Port.print(", ");
	Serial_Port.print(Angular_Velocity.Y_Value); Serial_Port.print(", ");
	Serial_Port.print(Angular_Velocity.Z_Value);

	Serial_Port.print(" / Incl. (deg): ");
	Serial_Port.print(Angle.X_Value); Serial_Port.print(", ");
	Serial_Port.print(Angle.Y_Value); Serial_Port.print(", ");
	Serial_Port.print(Angle.Z_Value);

	Serial_Port.print(" / ");
	Serial_Port.print("Rec. time (ms): ");
	Serial_Port.print(_Execution_Time);
	Serial_Port.println(" ms");
}